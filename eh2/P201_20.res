################################################################################
# This template should be used to generate an actual resource file for use by 
# one CT2ds device server controlling __ONE__ P201 or C208 card with the 
# following configuration:
#   - Master card = the card containing a master counter which in the resources
#                   is fixed to be the internal counter (Vct6IntCounter)
#   - No "Control channels" in use (i.e. no Ext Start/Stop, In/Out Gate) 
#   - Use 10 input channels which are associated with the counters with the
#     same index [Note: the hardware allows channels and counters to be 
#     "crossed", but to be "VCT6-like", the relation channel<->counter was
#     fixed so that counter i counts from input i]
#   - Internal counter #12 used a the Gate Generator for counting with given 
#                          number of internal clock ticks
#   - Counter #11:
#     On P201:
#       This counter has no corresponding input channel and can be marked as to
#       be potentially used by a precounter (which counts internal clock or
#       signals from the same channel as master or another valid input channel)
#       Vct6PreCounter: 11. But here we can also set 0. Then the device server 
#       in case the precounting in P201 is to be enabled will find and allocate
#       this counter as a precounter. As mentioned below with counter resources
#       the resources for this channel remain always commented for P201.
#     On C208:
#       Can use it as event counter (as it has a corresponding input channel)
#       or can require to be reserved for a potential precounter (which counts
#       internal clock or signals from the same channel as master or another
#       input channel). As mentioned below with counter resources, they should
#       be kept uncommendted for C208 independent of if precounter is to be 
#       used or not. 
# 
# To generate the actual resource file you have to:
#   1) replace the string bleng by the actual Device Server Personal Name
#   2) replace the string ID06 by the domain name in use on your machine
#   3) replace the string p201 by a key string that will be used
#      to build the VCT6-like channel devices. 
#      For example if you enter the string "ID06" for the domain name
#      and the string  "p201_0" (when card is P201) for the
#      channel device root name, the exported device names will be:
#      ID06/CardDev/00         (Card device) and
#      ID06/p201_0/1  --> ID06/p201_0/12 except ID06/p201_0/11 
#                              (Vct6 "Channel" devices)
#      If have C208 and if for channel device root name use "c208_0",
#      then the exported devices are:
#      ID06/CardDev/00         (Card device) and
#      ID06/c208_0/1  --> ID06/c208_0/12 or without ID06/c208_0/11,
#                         when counter 11 is "reserved" for potential precounter
#                              (Vct6 "Channel" devices)
#
#
# To use this device from Spec you have to:
#   1) Set the SCALERS config as:
#       SCALERS  DEVICE       ADDR <>MODE NUM         <>TYPE
#        YES     ID06/p201_0        TCP   12     ESRF VCT6 Counter/Timer
#             or ID06/c208_0
#   2) Define the time base counter as:
#       Number Name    Mnemonic <>Device Unit Chan  <>Use As   Scale Factor
#       0      Seconds sec      VCT6     0     12   timebase         1e+06
#   3) and the others counters as
#       N      toto    toto     VCT6     0 [1-10/11] counter             1
#
################################################################################
     
#------------------------------------------------------------------------
#                               General server resources
#------------------------------------------------------------------------

# DebugFlags (bit flags): 
#   0 - Fatal, 1 - Errors, 2 - Warnings, 3 - Params, 4 - Trace, 5 - Functions
#
# N.B. As soon as DebugFlags is > 0x7, the debug flag is set on in
#      device driver library and in ct.cpp classes library
#
#CLASS/CT2ds/bleng/DebugFlags:      0x3f
CLASS/CT2ds/id06_20/DebugFlags:      0x7


CLASS/CT2ds/id06_20/DeviceType:      CardDev

#------------------------------------------------------------------------
#			                        	Devices list
#------------------------------------------------------------------------
# Name(s) of the card-devices

### Warning: the last field is not free. It should represent the crate 
###          and unit number
CT2ds/lid063_0/device:	ID06/p201card_lid063/00 

#------------------------------------------------------------------------
#	            	Objects Resources
#------------------------------------------------------------------------

ID06/p201card_lid063/00/CrateNumber:       0          # taken from the name
ID06/p201card_lid063/00/UnitNumber:        0          # taken from the name

### Compatibility with VCT6 = 1 (!imperative, since other modes not implemented
### in the server)
ID06/p201card_lid063/00/Vct6Compat:        1

### Since none of "control channels" is used set all 4 ...Use to 0.
### In other resource files where 1 or more "control channels" is used,
### set for them the corresponding channel number (starting with 1).
ID06/p201card_lid063/00/Vct6InGateUse:     0
ID06/p201card_lid063/00/Vct6InGateLevel:   TTL 
ID06/p201card_lid063/00/Vct6OutGateUse:    10
ID06/p201card_lid063/00/Vct6OutGateLevel:  TTL 
ID06/p201card_lid063/00/Vct6ExtStartUse:   9
ID06/p201card_lid063/00/Vct6ExtStartLevel: TTL 
ID06/p201card_lid063/00/Vct6ExtStopUse:    0
ID06/p201card_lid063/00/Vct6ExtStopLevel:  TTL
# Since on P201 there are only 10 channels, select for internal counter
# counter 12 (the last one) [could select also 11, but is more convenient
# for C208 to use 11 contiguous inputs for event counter when precounter 
# is not used
ID06/p201card_lid063/00/Vct6IntCounter:    12
# channel 11:
#   C208: if want to use it for a precounter, then set Vct6PreCounter: 11, 
#         otherwise set Vct6PreCounter: 0 and counter 11 will be used as
#         event counter. If Vct6PreCounter: 0, will not be possible to find
#         a free counter for a precounter!
#   P201: can set Vct6PreCounter: 11, but even if set it to 0, device server
#         will find it to be free and will allocate/use it for a precounter
#         when it gets enabled. Here no wasting of counters can occur.
ID06/p201card_lid063/00/Vct6PreCounter:    11

### Key to build channel device names to be exported
### in this example 11/12 channel devices will be created and exported: 
### ID06/p201/1 -> ID06/p201/10,12 and 
### on C208 can have also ID06/p201/11 if no precounter.
ID06/p201card_lid063/00/ChanNameRoot:      ID06/p201_20


#------------------------------------------------------------------------
#  Event Counters = those that count from input channel
#  10(P201)/10 or 11(C208)
#------------------------------------------------------------------------

ID06/p201_20/1/CrateNumber:      0
ID06/p201_20/1/UnitNumber:       0
ID06/p201_20/1/ChNumber:         1 
ID06/p201_20/1/Mode:             COUNTER
ID06/p201_20/1/ChGate:           12
ID06/p201_20/1/Clock:            EXTERNAL
ID06/p201_20/1/InputLevel:       TTL

ID06/p201_20/2/CrateNumber:      0
ID06/p201_20/2/UnitNumber:       0
ID06/p201_20/2/ChNumber:         2
ID06/p201_20/2/Mode:             COUNTER
ID06/p201_20/2/ChGate:           12
ID06/p201_20/2/Clock:            EXTERNAL
ID06/p201_20/2/InputLevel:       TTL

ID06/p201_20/3/CrateNumber:      0
ID06/p201_20/3/UnitNumber:       0
ID06/p201_20/3/ChNumber:         3
ID06/p201_20/3/Mode:             COUNTER
ID06/p201_20/3/ChGate:           12
ID06/p201_20/3/Clock:            EXTERNAL
ID06/p201_20/3/InputLevel:       TTL

ID06/p201_20/4/CrateNumber:      0
ID06/p201_20/4/UnitNumber:       0
ID06/p201_20/4/ChNumber:         4
ID06/p201_20/4/Mode:             COUNTER
ID06/p201_20/4/ChGate:           12
ID06/p201_20/4/Clock:            EXTERNAL
ID06/p201_20/4/InputLevel:       TTL

ID06/p201_20/5/CrateNumber:      0
ID06/p201_20/5/UnitNumber:       0
ID06/p201_20/5/ChNumber:         5
ID06/p201_20/5/Mode:             COUNTER
ID06/p201_20/5/ChGate:           12
ID06/p201_20/5/Clock:            EXTERNAL
ID06/p201_20/5/InputLevel:       TTL

ID06/p201_20/6/CrateNumber:      0
ID06/p201_20/6/UnitNumber:       0
ID06/p201_20/6/ChNumber:         6
ID06/p201_20/6/Mode:             COUNTER
ID06/p201_20/6/ChGate:           12
ID06/p201_20/6/Clock:            EXTERNAL
ID06/p201_20/6/InputLevel:       TTL

ID06/p201_20/7/CrateNumber:      0
ID06/p201_20/7/UnitNumber:       0
ID06/p201_20/7/ChNumber:         7
ID06/p201_20/7/Mode:             COUNTER
ID06/p201_20/7/ChGate:           12
ID06/p201_20/7/Clock:            EXTERNAL
ID06/p201_20/7/InputLevel:       TTL

ID06/p201_20/8/CrateNumber:      0
ID06/p201_20/8/UnitNumber:       0
ID06/p201_20/8/ChNumber:         8
ID06/p201_20/8/Mode:             COUNTER
ID06/p201_20/8/ChGate:           12
ID06/p201_20/8/Clock:            EXTERNAL
ID06/p201_20/8/InputLevel:       TTL

ID06/p201_20/9/CrateNumber:      0
ID06/p201_20/9/UnitNumber:       0
ID06/p201_20/9/ChNumber:         9
ID06/p201_20/9/Mode:             COUNTER
ID06/p201_20/9/ChGate:           12
ID06/p201_20/9/Clock:            EXTERNAL
ID06/p201_20/9/InputLevel:       TTL

ID06/p201_20/10/CrateNumber:     0
ID06/p201_20/10/UnitNumber:      0
ID06/p201_20/10/ChNumber:        10 
ID06/p201_20/10/Mode:            COUNTER
ID06/p201_20/10/ChGate:          12
ID06/p201_20/10/Clock:           EXTERNAL
ID06/p201_20/10/InputLevel:      TTL

#------------------------------------------------------------------------
#    Counter #11
#------------------------------------------------------------------------
#
# On P201 Counter #11 should not be used if Vct6PreCounter != 0 or = 0,
# since already have internal counter (counter 12) and since counter 11
# has no input channel 11. So for P201 keep next 7 lines always commented.
#
# On C208 if Vct6PreCounter != 0, the counter #11 is "reserved" for potential
# use as precounter. In this case the Vct6-like Counter #11 device is not
# created even if 7 lines below are not commented. If Vct6PreCouter = 0,
# then Vct6-like counter #11 device is created. So for C208 keep next 7
# lines always uncommented.
#------------------------------------------------------------------------
#ID06/p201_10/11/CrateNumber:     0
#ID06/p201_10/11/UnitNumber:      0
#ID06/p201_10/11/ChNumber:        11 
#ID06/p201_10/11/Mode:            COUNTER
#ID06/p201_10/11/ChGate:          12
#ID06/p201_10/11/Clock:           EXTERNAL
#ID06/p201_10/11/InputLevel:      TTL

#------------------------------------------------------------------------
#    Internal counter number 12 used as gate generator for the 10/11 other ones
#------------------------------------------------------------------------

ID06/p201_20/12/CrateNumber:     0
ID06/p201_20/12/UnitNumber:      0
ID06/p201_20/12/ChNumber:        12 
ID06/p201_20/12/Mode:            GATE_GEN 
ID06/p201_20/12/ChGate:          MASTER 
ID06/p201_20/12/Clock:           INTERNAL
ID06/p201_20/12/InputLevel:      TTL
