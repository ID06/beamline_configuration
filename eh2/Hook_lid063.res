#------------------------------------------------------------------------
# Resource file for the Hook device server
#
# $Header: /segfs/dserver/classes++/hook/res/RCS/Hook.res,v 1.7 2003/02/17 14:42:42 ahoms Rel $
#
#------------------------------------------------------------------------

#------------------------------------------------------------------------
#				Devices list
#------------------------------------------------------------------------
#Insert here the name of the list and the name of the devices
Hook/Hook_lid063/device:	ID06/Hook_lid063/0

#------------------------------------------------------------------------
#				Default resources
#------------------------------------------------------------------------
CLASS/Hook/Hook_lid063/DebugFlags:		  0x3
CLASS/Hook/Hook_lid063/DriverDebug:		0x2

#------------------------------------------------------------------------
#   				Objects Resources 
#           (default Hook number is taken from the device name)
#------------------------------------------------------------------------
ID06/Hook_lid063/0/HookNumber:		0

