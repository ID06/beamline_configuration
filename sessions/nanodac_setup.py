
from bliss.setup_globals import *
from id06.controllers.id06_nanodac import Id06NanodacLoop

load_script("nanodac.py")

print("")
print("Welcome to your new 'nanodac' BLISS session !! ")
print("")
print("You can now customize your 'nanodac' session by changing files:")
print("   * /nanodac_setup.py ")
print("   * /nanodac.yml ")
print("   * /scripts/nanodac.py ")
print("")


real_loop = config.get("nanodacid06_loop")
loop = Id06NanodacLoop(real_loop)


def qq():
    print(f"power before quench: {loop.power:0.3f}")
    loop.power = 0
    loop.setpoint = 25
    loop.power = 0


loop.sensor_break_hold()
loop.plot()
