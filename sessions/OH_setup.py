from bliss.setup_globals import *
from bliss.common.utils import BOLD
from id06.presets.ID06SanityPreset import ID06SanityPreset
import numpy
from bliss import current_session
import time

load_script("OH.py")

from id06 import undulator_helper
from bliss.shell import getval

print("")
print("Welcome to your new 'OH' BLISS session !! ")
print("")
print("You can now customize your 'OH' session by changing files:")
print("   * /OH_setup.py ")
print("   * /OH.yml ")
print("   * /scripts/OH.py ")
print("")


SCAN_DISPLAY.auto = True


from bliss.shell.standard import plotselect as ps  # noqa


def bpm_in():
    print(BOLD("Move BPM into the beam"))
    icxpos = config.get("icxpos")
    icxpos.move("bpm")


def laser_in():
    print(BOLD("Move laser into the beam"))
    icxpos = config.get("icxpos")
    icxpos.move("laser")


def pico1_in():
    print(BOLD("Move pico1 into the beam"))
    icxpos = config.get("icxpos")
    icxpos.move("pico1")


def check_beam():
    machinfo = config.get("machinfo")
    current = machinfo.counters.current.value
    if current <= 0.1:
        raise RuntimeError(f"No machine current. Found {current} mA")
    frontend = config.get("fe")
    if frontend.is_closed:
        raise RuntimeError(f"fe is closed")
    bsh1 = config.get("sh1")
    if bsh1.is_closed:
        raise RuntimeError(f"sh1 is closed")

def pmococonfig():
  
  pmoco.comm("MODE POSITION")         # POSITION | INTENSITY | OSCILLATION
  pmoco.comm("INBEAM SOFT 1")         # INBEAM
                                      # INBEAM VOLT NORM BIP 10 NOAUTO
  pmoco.comm("SOFTBEAM 0")            # SOFTBEAM 5
  pmoco.comm("OUTBEAM VOLT NORM BIP 10 NOAUTO")
  pmoco.comm("OPRANGE 0 10 5")        # OPRANGE <Vmin> <Vmax> <Vsafety>
  pmoco.comm("SRANGE 4 6")            # SRANGE <Vmin> <Vmax> 

  pmoco.comm("SPEED 1 2")             # SPEED <scan speed V/sec> <move speed V/sec>
  pmoco.comm("TAU 3")                 # PID parameter: TAU 0.3
  # Autotuning: start tuning when an event happens
  # Use it if tuning is required after beam loss
  pmoco.comm("AUTOTUNE OFF BEAMLOSS")
  pmoco.comm("AUTOPEAK OFF")
  pmoco.comm("CLEAR   AUTORUN BEAMCHECK NORMALISE AUTORANGE INTERLOCK")
  pmoco.comm("INHIBIT OFF LOW")
  pmoco.comm("BEAMCHECK 0 0.2 1.024 0")

def bpmcen(voltage_allowance=0.2):
    from bliss.shell.standard import umvr
    from bliss.shell.standard import umv
    bpmz = config.get("bpmz")
    pmoco = config.get("pmoco")

    check_beam()

    saved_bmpz_pos = bpmz.position
    try:
        print("Current bpm readings:")
        s = ct(pmoco.counters.foutm)
        foutm = s.get_data()["pmoco:foutm"][0]
        print("foutm =", foutm)
        if abs(foutm) <= abs(voltage_allowance):
            print("That is already good enough.")
            return True
        retry = 10
        print(f"Improving (maximal {retry} times)...")
        for _ in range(retry):
            umvr(bpmz, foutm * 0.1)
            s = ct(pmoco.counters.foutm)
            foutm = s.get_data()["pmoco:foutm"][0]
            if abs(foutm) <= abs(voltage_allowance):
                break
        else:
            print("I am not getting anywhere. I abandon you in a mess.")
            print("At least I drive bpmx and bpmz back to where they came from.")
            umv(bpmz, saved_bmpz_pos)
            return False
    except:
        print("Reset bpmz position to the original")
        umv(bpmz, saved_bmpz_pos)
        raise

    return True


def mocowitch():
    from bliss.shell.standard import umv
    from bliss.shell.standard import loopscan

    check_beam()

    pmoco_m = config.get("pmoco_m")
    pmoco = config.get("pmoco")

    if abs(pmoco_m.position - 5) > 0.5:
        print("pmoco is {pmoco_m.position} {pmoco_m.unit}")
        result = getval.getval_yes_no("Do you want to continue with this current value of pmoco? ", False)
        if not result:
            return

    result = bpmcen()
    if not result:
        return

    print("Now counting 10 times to get average.")
    s=loopscan(10, 1, pmoco.counters.foutm)
    foutm = s.get_data()["pmoco:foutm"]
    print("foutm =", foutm)
    target = numpy.mean(foutm)
    print("Communicating this as the new setpoint...")
    pmoco.comm("SETPOINT %f" % target)
    print("Pitch feedback starting...")
    pmoco.tune()
    pmoco.view()

def moco():
    print(pmoco.__info__())
    print("        pmoco_m = %.3f V" % pmoco_m.position)
    
def uoptions(energy=None, odd: bool=True):
    """Display possible undulator settings for given photon energy, with
    possible preference for odd harmonics.

    Arguments:
        energy: The expected energy, if None the energy from the mono is used
        odd: If true select odd harmonics only
    """
    if energy is None:
        axis = config.get("energy")
        energy = axis.position
    return undulator_helper.uoptions(energy, odd)


mon1calc = config.get("mon1calc")
mon1sum = mon1calc.outputs["mon1sum"]


# sanity_preset = ID06SanityPreset()
# DEFAULT_CHAIN.add_preset(sanity_preset)

def ISDDsaving():
  current_session.disable_esrf_data_policy()
  yearInt = time.localtime()[0]
  SCAN_SAVING.base_path = '/data/id06-hxm/inhouse/%4d' % yearInt
  SCAN_SAVING.template  = '{session}/{date}'
  print("SCAN_SAVING.base_path:")
  print(SCAN_SAVING.base_path)
  print("SCAN_SAVING.filename:")
  print(SCAN_SAVING.filename)

def LVPsaving():
  current_session.enable_esrf_data_policy()
  #SCAN_SAVING.base_path='/data/id06-lvp/inhouse'
  #SCAN_SAVING.template='{proposal_dirname}/{beamline}/{proposal_session_name}/RAW_DATA/{collection_name}/{collection_name}_{dataset_name}'

print("SCAN_SAVING.base_path:")
print(SCAN_SAVING.base_path)
print("SCAN_SAVING.filename:")
print(SCAN_SAVING.filename)
print()
print("TYPE \"ISDDsaving()\" TO CHANGE SCAN_SAVING FOR XOG !!!!!!!!!!")
