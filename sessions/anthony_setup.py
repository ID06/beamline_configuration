
from bliss.setup_globals import *

load_script("anthony.py")

print("")
print("Welcome to your new 'anthony' BLISS session !! ")
print("")
print("You can now customize your 'anthony' session by changing files:")
print("   * /anthony_setup.py ")
print("   * /anthony.yml ")
print("   * /scripts/anthony.py ")
print("")