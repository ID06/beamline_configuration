
from bliss.setup_globals import *

load_script("session_pilatus.py")

print("")
print("Welcome to your new 'session_pilatus' BLISS session !! ")
print("")
print("You can now customize your 'session_pilatus' session by changing files:")
print("   * /session_pilatus_setup.py ")
print("   * /session_pilatus.yml ")
print("   * /scripts/session_pilatus.py ")
print("")