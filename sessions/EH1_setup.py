
from bliss.setup_globals import *

load_script("EH1.py")

print("")
print("Welcome to your new 'EH1' BLISS session !! ")
print("")
print("You can now customize your 'EH1' session by changing files:")
print("   * /EH1_setup.py ")
print("   * /EH1.yml ")
print("   * /scripts/EH1.py ")
print("")