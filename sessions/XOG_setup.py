from bliss.setup_globals import *
import numpy as np
import sys
import os
import time
from bliss import current_session
from bliss.shell.standard import plotselect as ps

load_script("XOG.py")

print("")
print("Welcome to your new 'XOG' BLISS session !! ")
print("")
print("You can now customize your 'XOG' session by changing files:")
print("   * /XOG_setup.py ")
print("   * /XOG.yml ")
print("   * /scripts/XOG.py ")
print("")

from id06.eh2presets import (
    CountFastShutterPreset, 
    FScanFastShutterPreset,
)

print("Setup fast shutter in the chain acquisition.\n")
_countshut_XOG = CountFastShutterPreset(eh1fs)
DEFAULT_CHAIN.add_preset(_countshut_XOG, "fastshutter")


current_session.disable_esrf_data_policy()
#pcogold.select_directories_mapping('inhouse')
#pcogold.roi_counters.set("total", (0,0,2560,2160))


load_script("session_turret.py")

print("Loading macros for speckle tracking ...")
user_script_load("/users/blissadm/local/beamline_configuration/sessions/scripts/ST_ID06.py","st")
print("Loading T.Roth convenience macros ...")
user_script_load("/data/optics/refractive_optics/macros/tr.py","tr")
print("Loading ID06 EH1 convenience macros ...")
user_script_load("/users/blissadm/local/beamline_configuration/sessions/scripts/eh1.py","eh1")


#Include some direct access to OH systems
#load_script("mono.py")
#user_script_load("/data/bm05/inhouse/xog-refropt/macros/xsvt-mono.py", "kev")

#2022-04-25
#def activate_edgehs():
#    multiplexer.switch("CAMERA", "PCO")
#    MGEdgeHS.set_active()

#def activate_hard_triggering():
#    print("Hard trigger activated")
#   DEFAULT_CHAIN.set_settings(default_acq_chain['chain_config'])
#   
#def activate_soft_triggering():
#   DEFAULT_CHAIN.set_settings([])  

def pstot():
    plotselect(pco_eh1.counters.full_avg)

def psstd():
    plotselect(pco_eh1.counters.full_std)
    
    
#Temporary hack until BLISS bug is fixed
#def fix_motor(motor):
#    motor.settings.set("state", motor.hw_state)

#def activate_pco(hard_trigger=True):
#    cam_obj = pcogold
#    multiplexer2.switch("CAMERA", "SPARE")
#    MGPco.set_active()
#    if hard_trigger:
#        activate_hard_triggering()
#        multiplexer2.switch("SOURCE", "P201")
#    else:
#        activate_soft_triggering()
#    cam_obj.roi_counters["total"] = cam_obj.get_current_parameters()["image_roi"]
#    plotselect(cam_obj.counters.total_avg)

# Activate HW trigger
DEFAULT_CHAIN.set_settings(chain_xog_hw_trig["chain_config"])


#def activate_basler():
    #activate_soft_triggering()
    #MGBasler.set_active()
    #DEFAULT_CHAIN.set_settings([])

#def activate_hard_triggering():
#    print("Hard trigger activated")
#    DEFAULT_CHAIN.set_settings(default_acq_chain['chain_config'])
#    if str(ACTIVE_MG.available).find("201") >= 0:
#        print("Disabled P201 counter channels")
#        ACTIVE_MG.disable('*p201*')
    
#def activate_soft_triggering():
#    print("Soft trigger activated")
#    DEFAULT_CHAIN.set_settings([])  
#    if str(ACTIVE_MG.available).find("201") >= 0:
#        print("Enabling P201 counter channels")
#        ACTIVE_MG.enable('*p201*')

#def fs(t=1):
    #fshut.fsextoff()
    #fshut.open()
    #sleep(t)
    #fshut.close()
    #fshut.fsexton()



