
from bliss.setup_globals import *

load_script("session_keithley.py")

print("")
print("Welcome to your new 'session_keithley' BLISS session !! ")
print("")
print("You can now customize your 'session_keithley' session by changing files:")
print("   * /session_keithley_setup.py ")
print("   * /session_keithley.yml ")
print("   * /scripts/session_keithley.py ")
print("")