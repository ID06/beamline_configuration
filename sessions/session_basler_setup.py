
from bliss.setup_globals import *

load_script("session_basler.py")

print("")
print("Welcome to your new 'session_basler' BLISS session !! ")
print("")
print("You can now customize your 'session_basler' session by changing files:")
print("   * /session_basler_setup.py ")
print("   * /session_basler.yml ")
print("   * /scripts/session_basler.py ")
print("")