
from bliss.setup_globals import *

load_script("LVP.py")

print("")
print("Welcome to your new 'LVP' BLISS session !! ")
print("")
print("You can now customize your 'LVP' session by changing files:")
print("   * /LVP_setup.py ")
print("   * /LVP.yml ")
print("   * /scripts/LVP.py ")
print("")

from id06.eh2presets import (
    CountFastShutterPreset, 
    FScanFastShutterPreset,
)


print("Setup fast shutter ...")
_countshut_eh2 = CountFastShutterPreset(eh1fs)
DEFAULT_CHAIN.add_preset(_countshut_eh2, "fastshutter")

#_scanshut_eh2 = FScanFastShutterPreset(eh1fs)
#scan_eh2.add_chain_preset(_scanshut_eh2, "fastshutter")
