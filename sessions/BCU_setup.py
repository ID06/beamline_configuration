
from bliss.setup_globals import *

load_script("BCU.py")

print("")
print("Welcome to your new 'BCU' BLISS session !! ")
print("")
print("You can now customize your 'BCU' session by changing files:")
print("   * /BCU_setup.py ")
print("   * /BCU.yml ")
print("   * /scripts/BCU.py ")
print("")