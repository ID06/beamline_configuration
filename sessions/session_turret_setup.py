
from bliss.setup_globals import *

load_script("session_turret.py")

print("")
print("Welcome to your new 'session_turret' BLISS session !! ")
print("")
print("You can now customize your 'session_turret' session by changing files:")
print("   * /session_turret_setup.py ")
print("   * /session_turret.yml ")
print("   * /scripts/session_turret.py ")
print("")