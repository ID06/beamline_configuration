import numpy as np

#########################################################################################
### shortcuts for icx translation, that serves alignmnet laser, Basler2x and Ionisation chamber
###  TRoth 26.3.2024

def icxState():
    icxPos=icx.position
    if abs(icxPos+50)<1:
        print("icx= %.1f  Alignmnet laser in beam." % icxPos)
    elif abs(icxPos+13.5)<1:
        print("icx= %.1f  Basler 2x in beam." % icxPos)
    elif abs(icxPos-45.9)<5:
        print("icx= %.1f  ionisation chamber (for pico1) in beam." % icxPos)
    else:
        print("icx= %.1f  undefined. \nNote: -50 for laser;  -13.5 for Baselr;  45.9 for ionisation chamber." % icxPos)

def icxHelp():
    wm(icx)
    print("Note: -50 for laser  \n     -13.5 for Baselr\n     45.9 for ionisation chamber.")

def laserIn():
    print("Was: ",end=""); icxState()
    umv(icx,-50)
    print("EH1 alignment laser is on beam axis")

def laserOut():
    icIn()
    
def bas2In():
    print("Was: ",end=""); icxState()
    umv(icx,-13.5)
    print("EH1 Basler2x (for eh1bpm) is on beam axis")

def bas2Out():
    icIn()
    
def icIn():
    print("Was: ",end=""); icxState()
    umv(icx,45.9)
    print("EH1 ion chamber (for pico1) is on beam axis")

def pico1In():
    icIn()

def picoIn():
    icIn()


#########################################################################################
### shortcuts for rotating diode on end of MOTB
###  TRoth 26.3.2024
    
def diode2In():
   umv(diode,65)
   print("rotating Diode2 at end of MOTB is in!")

def diode2Out():
   umv(diode,0)
   print("rotating Diode2 at end of MOTB, is out!")

#########################################################################################
### shortcuts for rotating decoherer into and out of beam (after ionchamber and fast shutter)
###  TRoth 26.3.2024
    
def decohIn():
   umv(decoth,0)

def decohOut():
   umv(decoth,23)

#########################################################################################
### shortcuts for removing all obstacles before putting beam pipes in towards EH2
###  TRoth 26.3.2024
    

def toLVP():
    icIn()
    diode2Out()
    umv(decohth,23,yy2,0,y2,-200,y3,100)
    umv(s4hg,0.5)

    
def beam():
    if fe.is_closed:
        fe.open()
        print("Front End status is ", fe.state)
    elif fe.is_open:
        print("Front End is open")
    else:
        print("CHECK FRONT END STATUS")
    if fe.mode=="MANUAL":
       print("Setting FE to automatic mode")
       fe.mode="AUTOMATIC"        
    icIn()

    # DISABLE / CLOSED / OPEN
    shstate = str(bsh1.state).split(".")[-1]
    print(shstate)
    if shstate == "OPEN":
        print("Shutter for eh1 is already open")
        return
    if shstate in ["DISABLED", "DISABLE"]:
        print("Currently disabled. Waiting for end of search ...")
        while shstate == "DISABLED":
            sleep(.1)
            shstate = str(bsh1.state).split(".")[-1]

    print("Trying to open the shutter...")
    ctr = 0
    while shstate != "OPEN":
        try:
            bsh1.open(quiet=True)
        except:
            bsh1.open()
            print("edit /users/blissadmin/local/bliss.git/bliss/controllers/tango_shutter.py !!")
        if (ctr > 2):
            sleep(.25)
            shstate = str(bsh1.state).split(".")[-1]
        else:
            shstate = str(bsh1.state).split(".")[-1]
        ctr += 1

    print("Shutter open.")
    ct(.1)
