import numpy as np
import os
import pylab
import time
import glob
from scipy.optimize import curve_fit
from bliss.config.settings import ParametersWardrobe  # get equivalent to global variables
from bliss import current_session                     # to enable custom file naming

##################################################################################
# Global variables ###############################################################
##################################################################################

def initGlobals():
    lensGlob = ParametersWardrobe('lens-globals-id06')
    # This gets from Redis a set of variables called lens-globals-id06.
    # There might be many defined, there might be none.
    lensGlob.add('press', '2D lens press')

def deleteGlobals():
    lensGlob = ParametersWardrobe('lens-globals-id06')
    lensGlob.purge()

def show_press_info():
    showGlobals()

def showGlobals():
    lensGlob = ParametersWardrobe('lens-globals-id06')
    print(lensGlob.__info__())

def radius(newradius=None):
    lensGlob = ParametersWardrobe('lens-globals-id06')
    if newradius==None:
       try:
           print("punch radius is %d um" % lensGlob.radius)
       except:
           print("radius of punches not yet defined.")
           print("type \"radius(<radius in um>)\" to define punch radius")
       return
    if type(newradius)==int or type(newradius)==float:
        lensGlob.add('radius',newradius)
    else:
           print("radius of punches needs to be integer or float.")
           print("type \"radius(<radius in um>)\" to define punch radius")

def lensNumber(newN=None):
    lensGlob = ParametersWardrobe('lens-globals-id06')
    if newN==None:
       try:
           print("number of last pressed lens is %d" % lensGlob.lastLensN)
       except:
           print("number of pressed lenses of not yet defined.")
           print("type \"lensNumber(<lensN>)\" to define number of pressed lenses")
       return
    if type(newN)==int:
        lensGlob.add('lastLensN',newN)
    else:
           print("radius of pressed lenses needs to be integer.")
           print("type \"lensNumber(<lensN>)\" to define number of pressed lenses")

def frameType(newFrameType=None):
    lensGlob = ParametersWardrobe('lens-globals-id06')
    if newFrameType==None:
       try:
           print("frame type for next lens is %s" % lensGlob.frameType)
       except:
           print("frame type is not yet defined.")
           print("type \"frameType(<frameType>)\" to define next frame type")
       return
    if type(newFrameType)==str:
        lensGlob.add('frameType',newFrameType)
    else:
           print("frame type needs to be str.")
           print("type \"frameType(<frameType>)\" to define next frame type")

def holePos(newHolePos=None):
    lensGlob = ParametersWardrobe('lens-globals-id06')
    if newHolePos==None:
       try:
           print("position of venting hole is %s" % lensGlob.holePos)
       except:
           print("frame type is not yet defined.")
           print("type \"holePos(<holePos>)\" to define next position of venting hole")
       return
    if type(newHolePos)==str:
        lensGlob.add('holePos',newHolePos)
    else:
           print("hole position of venting hole needs to be str.")
           print("type \"holePos(<holePos>)\" to define next position of venting hole")

def lpunch(lpunchName=None):
    lensGlob = ParametersWardrobe('lens-globals-id06')
    if lpunchName==None:
       try:
           print("current left punch is %s" % lensGlob.lpunch)
       except:
           print("left punch name is not yet defined.")
           print("type \"press.lpunch(<lpunchName>)\" to specify used left punch")
       return
    if type(lpunchName)==str:
        lensGlob.add('lpunch',lpunchName)
    else:
           print("name of punhc needs to be str.")
           print("type \"press.lpunch(<lpunchName>)\" to specify used left punch")

def lpunch(lpunchName=None):
    lensGlob = ParametersWardrobe('lens-globals-id06')
    if lpunchName==None:
       try:
           print("current left punch is %s" % lensGlob.lpunch)
       except:
           print("left punch name is not yet defined.")
           print("type \"press.lpunch(<lpunchName>)\" to specify used left punch")
       return
    if type(lpunchName)==str:
        lensGlob.add('lpunch',lpunchName)
    else:
           print("name of punhc needs to be str.")
           print("type \"press.lpunch(<lpunchName>)\" to specify used left punch")
           
def rpunch(rpunchName=None):
    lensGlob = ParametersWardrobe('lens-globals-id06')
    if rpunchName==None:
       try:
           print("current left punch is %s" % lensGlob.rpunch)
       except:
           print("left punch name is not yet defined.")
           print("type \"press.rpunch(<rpunchName>)\" to specify used left punch")
       return
    if type(rpunchName)==str:
        lensGlob.add('rpunch',rpunchName)
    else:
           print("name of punhc needs to be str.")
           print("type \"press.rpunch(<rpunchName>)\" to specify used left punch")
           



#########################################################
    
print("-------------------------------------")
print("----- ST_ID06.py being loaded -------")
print("-------------------------------------\n")

initGlobals()
lensGlob = ParametersWardrobe('lens-globals-id06')
sleep(0.2)
