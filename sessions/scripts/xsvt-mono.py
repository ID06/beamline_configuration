from tabulate import tabulate
from bm05.scan_math import GaussianFit, calc_residuals, leastsq
from bliss.common.plot import display_motor
from bliss.common.utils import ShellStr, grouped_with_tail
from scipy.interpolate import interp1d
import numpy

class Mrolst:
  _values = numpy.array((
    (5, -0.0299),
    (6, -0.0346),
    (7, -0.0389),
    (8, -0.0434),
    (9, -0.0484),
    (10, -0.0524),
    (11, -0.0574),
    (12, -0.0624),
    (13, -0.0674),
    (14, -0.0724),
    (15, -0.0774),
    (16, -0.0824),
    (17, -0.0874),
    (18, -0.0924),
    (19, -0.0974),
    (20, -0.10243),
    (21, -0.1084),
    (22, -0.113),
    (23, -0.1183),
    (24, -0.1234),
    (25, -0.1284),
    (26, -0.1334),
    (27, -0.1384),
    (28, -0.1434),
    (29, -0.1484),
    (30, -0.1534),
    (31, -0.1594),
    (32, -0.1644),
    (33, -0.1694),
    (34, -0.1744),
    (35, -0.1794),
    (36, -0.1844),
    (37, -0.1904),
    (38, -0.1954),
    (39, -0.2004),
    (40, -0.2064),
  ))

  def __init__(self):
    self.f = interp1d(Mrolst._values[:, 0], Mrolst._values[:, 1],kind='quadratic')

  def getAt(self, energy):
    return self.f((energy, ))[0]

  def __call__(self, *args):
    if len(args) != 1:
      raise ArgumentError("Give me an energy.")
    return self.getAt(args[0])

mrolstcalc = Mrolst()

######################################################################################

def rockWidth(E,unit="arcsec"):
  _values = numpy.array((
    (5, 17.81),
    (8, 10.21),
    (10, 7.99),
    (12, 6.57),
    (15, 5.20),
    (17, 4.56),
    (20, 3.857),
    (25, 3.070),
    (30, 2.551),
    (35, 2.183),
    (40, 1.907),
    (50, 1.524),
    (60, 1.269),
    (70, 1.087),
    (80, 0.951),
  ))
  #table in arcsec
  f=interp1d(_values[:, 0], _values[:, 1], kind='quadratic')
  rwarray=numpy.reshape(f(E),[1])
  rwf=rwarray[0]
  if str.lower(unit) in ["arcsec","arcs","Arcsec"]:
     return rwf
  elif str.lower(unit) in ["deg","degs","degree","degrees"]:
     return rwf/3600.0
  elif str.lower(unit) in ["rad","radian","rads","radians"]:
     return rwf*4.8481e-6
  elif str.lower(unit) in ["mrad","milliradian","milli-radian","mrads","milliradians","milli-radians"]:
     return rwf*4.8481e-3
  elif str.lower(unit) in ["urad","microradian","micro-radian","urads","microradians","micro-radians"]:
     return rwf*4.8481
  else:
      print("Unknown unit %s" % unit)
  return
   
######################################################################################    

def _whereE():
    """
    E = 10 keV          Wavelength = 20 nm     mono =   3 deg       detuning = 10%
    mtrans =  23 mm     mpitst = 10 mm         mrolst = 13          mtilt2 = 15 arcsec
    """
    data = []
    myE=bm05mono.bragg2energy(mono.position) 
    data.append(f"E = {myE :.4f} {monoE.unit}")
    data.append(f"Wavelength = {1.24/myE :.4f} nm")
    data.append(f"mono = {mono.position :.4f} {mono.unit}")
    #data.append(f"detuning = {getE.detuning * 100 :.1f} %")
    for motor in (mtrans, mpitst, mrolst, mtilt2):
        data.append(f"{motor.name} = {motor.position: .4f} {motor.unit}")
    return ShellStr(tabulate(grouped_with_tail(data,4)))
    
def getE():
    print(_whereE()) 
    myE=bm05mono.bragg2energy(mono.position) 
    #myE=monoE.position
    print("ENERGY NOW:  %.3f keV"  % myE)
    print("Expected rocking curve width:")
    print("   %.2f arcsec (piezo)   " % rockWidth(myE,unit="arcsec"))
    print("   %.5f degree (stepper) " % rockWidth(myE,unit="deg"))
    print("   %.2f urad             " % rockWidth(myE,unit="urad"))

def moveE(newE,do_dm=False,counter=pico1,detune=20,mode="hopeful",diodeIn=False,diodeOut=False):
    #changes bragg and fixed offset:
    monoE.move(newE) 

    #new roll position
    try:
       mrolst_pos = mrolstcalc(newE)
       print("Calculated position for mrolst at " + str(newE) + " keV is " + str(mrolst_pos))
    except:
       print("Can not interpolate good value for roll motor. Take roll for 40 keV")
       mrolst_pos = mrolstcalc(40)
       print("Calculated position for mrolst at " + str(40) + " keV is " + str(mrolst_pos))
    umv(mrolst,mrolst_pos)

    #rocking curve scan
    if do_dm:
       if diodeIn:
            #ibt.diode()
            umv(diode1,-30)
       dm(counter=counter,detune=detune,mode=mode)
       if diodeOut:
            #ibt.free()
            umv(diode1,0)
           
def analyseRock(scan,counter):       
    sdata = scan.get_data()
    try:
       X = sdata[mtilt2]   #piezo motor???
    except:
       X = sdata[mpitst]   #stepper motor
    Y = sdata[counter]
    peak_pos=peak()
    last_pos=X[-1]
    first_pos=X[0]
    stepsize=X[1]-X[0]
    #fit
    gfit = GaussianFit(X, Y)
    # If Y is too noisy, one can pass instead a moving average of Y
    # for the inital guess.
    p = leastsq(calc_residuals, gfit.init(), args = (gfit, ), xtol = 5e-3, maxfev = 25)
    # leastsq returns a tuple of 2 elements: the numpy array + a number
    p = p[0] # getting the numpy array
    # Fit is working if the difference in integral is small enough
    # and the peak position is within X range
    epsilon = numpy.abs(Y.sum() - gfit.evaluate(p).sum()) / Y.sum()
    if (epsilon > 0.1) or (gfit.peakpos(p) > X.max()) or (gfit.peakpos(p) < X.min()):
        print("Cannot fit a gaussian to the data. We will run a scan on mpitst...")
        print(counter.name)
        print(peak())
        bestPos=peak(counter)
        fwhmFit=-1
        state="incomplete"
    else:
        print("Scan peak: %.6f" % peak())
        print("Scan cen:  %.6f" % cen())
        bestPos = gfit.peakpos(p)
        print("Scan Gauss Fit: %.6f" % bestPos)
        fwhmFit = gfit.fwhm(p)
#        print("Bliss FWHM:     %.6f" % _scan_calc("fwhm"))
#        print("Bliss FWHM:     %.6f" % fwhm())
        print("Gauss Fit FWHM: %.6f" % fwhmFit)
        state="good"
    return state,bestPos,fwhmFit

def dm(counter=pico1,detune=20,mode="hopeful",diodeIn=True,diodeOut=True):
    """
    mode="hopeful"  starts with piezos and might stop then
    mode="tweak"    only does a limited stroke piezo scan and stops
    mode="scratch"  moves piezo to mid-stroke, scans then stepper and again piezo
    piezo motor:  mtilt2
    stepper motor: mpitst
    """
    while fe.is_closed or eh1.is_closed or srcur.value<2:
       print("Waiting for beam....")
       fe.open()
       eh1.open()
       sleep(10)
    
    myE=bm05mono.bragg2energy(mono.position) #monoE.position
    wRock_arc=rockWidth(myE,unit="arcsec")
    wRock_deg=rockWidth(myE,unit="deg")
    print("Current energy: %.3f keV    rocking curve width %.3f arcsec" % (myE,wRock_arc))

    plotselect(counter)

    if diodeIn:
            #ibt.diode()
            umv(diode1,-30)

    if mode=="hopeful" or mode=="tweak":
       ### START WITH PIEZOS ####
       now=mtilt2.position
       if mode=="hopeful":
          neg=-1.0*(now-3)
          pos=(40-now)
          print(wRock_arc)
          nSteps=int(38.0/wRock_arc*5)  # 5 points over FWHMc
          nSteps=min(200,nSteps)
          nSteps=max(30,nSteps)
       else:
          #mode=="tweak"
          neg=-.75*wRock_arc
          if now+neg<=2:
              neg=3-now
          pos= .75*wRock_arc
          if now+pos>=52:
              pos=51-now
          nSteps=30  # 20 points over 1.5*FWHM

       ct(.1,counter) #first data point often bad
       result=dscan(mtilt2,neg,pos,nSteps,.1,counter)

       state,bestPos,fwhm=analyseRock(result,counter)
       lastMotor=mtilt2
       
    if mode=="hopeful" and state!="good":
       #continue with stepper scan
       mode="scratch"
       
    if mode=="scratch":
       umv(mtilt2,25)
       ### SCAN WITH STEPPER ####
       now=mpitst.position

       neg=-0.01
       pos=0.01
       stroke=pos-neg
       nSteps=int(stroke/wRock_deg*6) # 6 points over FWHM
       nSteps=min(250,nSteps)  #maximum steps 250
       nSteps=max(30,nSteps)   #minimum steps 30

       ct(.1,counter) #first data point often bad
       result=dscan(mpitst, neg, pos, nSteps, 0.1, counter)
       lastMotor=mpitst
       state,bestPos,fwhm=analyseRock(result,counter)

       if state!="good":
          result=dscan(mpitst, neg*2.5, pos*2.5, int(nSteps*2.5), 0.1, counter)
          state,bestPos,fwhm=analyseRock(result,counter)
          if state!="good":
              print("Can not get to a resonable peak.")
              print("I ABANDON THE PROCEDURE")
              return

       #stepper scan was good by now
       umv(mpitst,bestPos)
       now=mtilt2.position
       if (stroke/nSteps*10)>fwhm:
           #scan with stepper not fine enough
           ### SCAN WITH PIEZO ###
           neg=-3.0*wRock_arc
           if now+neg<=2:
              neg=3-now
           pos= 3.0*wRock_arc
           if now+pos>=62:
              pos=61-now
           nSteps=40  # 40 points over 2*FWHM
          
           result=dscan(mtilt2,neg,pos,nSteps,.1,counter)
           lastMotor=mtilt2
           state,bestPos,fwhm=analyseRock(result,counter)
    
    if detune!=0:
        if detune>1:
           print("detuning in percentage of Gauss Fit fwhm")
           final_position = bestPos + fwhm * detune / 100.0 * 0.5
           detuneType="fitted width"
        else:
           print("detuning to percentage of theoretical rocking curve width")
           final_position = bestPos + wRock_arc * detune  * 0.5
           detuneType="theory width"
    else:
        final_position = bestPos
        detuneType=""
        
    umv(lastMotor,final_position)
    print(f"Centre is on {bestPos} fwhm is {fwhm} detuned position by {detune} will be {final_position}")
    if detune!=0:
       display_motor(lastMotor, scan=result, position=final_position, label=f'detuned %{detune: .1f} {detuneType}')
       # ^ adds line to last scan in flint
    else:
       display_motor(lastMotor, scan=result, position=final_position, label=f'not detuned')       
    if diodeOut:
            #ibt.free()
            umv(diode1,0)


def psAlign():
   print("Going to 60 keV")
   print("diode should be installed on y-stage, e.g. pico3 on y3.")
   umv(diode1,0)
   moveE(60,True,pico3,mode="scratch",detune=0)

   print("open secondary slits...")
   print("secondary slits gap: %.2f x %.2f at offset %.1f / %.1f" % (sshg.position, ssvg.position, ssho.position, ssvo.position))
   umv(ssho,0,ssvo,0)
   if sshg.position<55:
      umv(sshg,55)
   if ssvg.position<10:
      umv(sshg,10)

   print("currennt primary slits:")     
   print("primary slits gap: %.2f x %.2f at offset %.1f / %.1f" % (pshg.position, psvg.position, psho.position, psvo.position))
   umv(psvo,0,psho,0)
   umv(pshg,10,psvg,2)

   dscan(psvo,-3,3,30,.1,pico3)
   print("Scan FWHM is %.2f mm" % fwhm())
   
   if fwhm()>2 and fwhm()<3.5:
      print("That's a reasonable FWHm when vertically scanning with a 2mm wide primary slit at 60 keV.")
      goto_com()
      psvo.position=0
   else:
      print("That's NOT a reasonable FWHM. Something looks bad.")

   umv(psvg,2,pshg,2)

   print("scanning psho and pico3/y3 horizontal offset ...")
   print("distance ratio x_pico3/x_ps = 1.68")
   d2scan(psho,-20,20,y3,-20*1.68,20*1.68,40,.1,pico3)

   if fwhm()>20 and fwhm()<50:
      print("That's a reasonable FWHm when horizontally scanning with a 2mm wide primary slit at 60 keV.")
      goto_com()
      print("You might want to set this or tweaked position of psho to zero.")
   else:
      print("That's NOT a reasonable FWHM. Something looks bad.")

   umv(pshg,2,psvg,2) 


