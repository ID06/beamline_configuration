import time

print("\n\n Some fonctions used for the Turret are:")
print("\t\t\t\t\t turretPosition")
print("\t\t\t\t\t setx_plus")
print("\t\t\t\t\t setx_minus")

def setx_plus(t=None):
    print("\t turret was:")
    myTurretPosition()
    wcid06c.set("turnTurret",1,0)
    t = 1 if t is None else t
    print(f"\t Time pulse is {t}s.")    
    time.sleep(t)
    wcid06c.set("turnTurret",0,0)
    print("\t turret now is:")
    myTurretPosition()

def setx_minus(t=None):
    print("\t turret was:")
    myTurretPosition()
    wcid06c.set("turnTurret",0,1)
    t = 1 if t is None else t
    print(f"\t Time pulse is {t}s.")    
    time.sleep(t)
    wcid06c.set("turnTurret",0,0)
    print("\t turret now is:")
    myTurretPosition()
    
def myTurretPosition():
    positions = wcid06c.get("x4inPlace","x10inPlace","x20inPlace")
    print(f"\t\t\tx4inPlace: {positions[0]} | x10inPlace: {positions[1]} | x20inPlace: {positions[2]}")



