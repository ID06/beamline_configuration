import numpy as np
import os

SCAN_SAVING.base_path='/data/id30b/inhouse/XOG/2DLenses'

def saveAlign():
    SCAN_SAVING.template="align"
    SCAN_SAVING.images_path_template='scan{scan_number}'

def save(myfolder,myprefix):
    SCAN_SAVING.template=myfolder
    SCAN_SAVING.images_path_template='%s{scan_number}' % myprefix


def wireAtZero():
    umv(diagx,3, diagy,6, diagz,-7.25)
    umv(diagx,0.285)

def wireAt20():
    umv(diagx,20, diagy,6, diagz,-7.25)

def baslerAtZero():
   umv(diagx,3,diagy,-13.55,diagz,-5.6)
   umv(diagx,0)

def baslerAt25():
   umv(diagx,25,diagy,-13.55,diagz,-5.6)

def baslerX(xpos=25):
   umv(diagx,xpos,diagy,f(xpos),diagz,-5.6)

def f(x):
   return -13.775+0.009107*x

def baslerscan(x1,x2,pts,cttime=0.0005):
   a2scan(diagx,x1,x2,diagy,f(x1),f(x2),pts,cttime)
   
def wscanhAtZero(repeats=3):
    umv(wirez,0.5,wirey,0,diagy,24)
    for ii in range(repeats):
       ascan(wirey,-0.3,0.3,300,.2)
       ascan(wirey,-0.2,-0.0,200,.2)
       ascan(wirey,0.0,0.2,200,.2)
       umv(wirey,0)

def wscanvAtZero(repeats=3):
    umv(wirey,0.5,wirez,0,diagy,24)
    for ii in range(repeats):
       ascan(wirez,-0.3,0.3,300,.2)
       ascan(wirez,-0.2,-0.05,150,.2)
       ascan(wirez,0.05,0.2,150,.2)
       umv(wirez,0)

def baslerAtE(myE,cttime=0.0005):
    save("N9_energy","sct%.1fkeV" % myE)
    move_energy(myE)
    tfmad.set_all(False)
    tfmad.set_in(6)
    tfmad.set_in(7)
    sleep(1)
    sct(cttime)
       
def baslerEscan(E1,E2,Esteps):
    save("N9_Escan","N9_Escan")
    cttime=0.0002
    for EE in np.linspace(E1,E2,Esteps):
        print(EE)
        move_energy(EE)
        tfmad.set_all(False)
        tfmad.set_in(7)
        tfmad.set_in(6)
        sleep(1)
        result=ct(cttime)
        Ib=result.get_data("intensity")[0]
        if Ib>4000:
            cttime=cttime/2.0
        if Ib<1500:
            cttime=cttime*2.0
        sct(cttime)


        
    
def bfwhm():
    mg1.enable_all()
    mg1.disable("kdiode:kdiode") 
    plotselect('xrayeye:bpm:fwhm_y','xrayeye:bpm:fwhm_x')  

def bpos():
    mg1.enable_all()
    mg1.disable("kdiode:kdiode") 
    plotselect('xrayeye:bpm:y','xrayeye:bpm:x')  

def bint():
    mg1.enable_all()
    mg1.disable("kdiode:kdiode") 
    plotselect('xrayeye:bpm:intensity')  

def td():
    os.system("td mc_carthy")
    
def diode():
    mg1.enable_all()
    mg1.disable('xrayeye:bpm:acq_time', 'xrayeye:bpm:intensity', 'xrayeye:bpm:fwhm_x', 'xrayeye:bpm:y', 'xrayeye:bpm:fwhm_y', 'xrayeye:image', 'xrayeye:bpm:x')
    plotselect(kdiode.counters.kdiode)
    
def beam():
   fshutter.open()
   safshut1.open()
   shstate=str(safshut2.state).split(".")[-1]
   if shstate=="OPEN":
      print("Shutter for eh1 is already open")
      return
   if shstate=="DISABLED":
      print("Currently disabled. Waiting for end of search ...")
      while shstate=="DISABLED":
        sleep(.1)
        shstate=str(safshut2.state).split(".")[-1]
   print("Opening the shutter...")
   ctr=0
   while shstate!="OPEN":
      safshut2.open()
      if (ctr>2):
         sleep(.25)
         shstate=str(safshut2.state).split(".")[-1]
      else:
         shstate=str(safshut2.state).split(".")[-1]
      ctr+=1
 

 
        
