import numpy as np
import os
import time
from bliss.config.settings import ParametersWardrobe    #get equivalent to global variables
from bliss import current_session                       #to enable custom file naming
import pylab
#from bliss.common.shutter import BaseShutter, BaseShutterState

################################################################################
##### COMMANDS AND GLOBAL VARIABLES   ##########################################
################################################################################
################################################################################

def help():
   print("Available commands")
   os.system("cat /data/optics/refractive-optics/macros/XOGatID15B.py | grep '^def '")
   print("Global variables")
   showGlobals()
   print("required global variables")
   requiredGlobals()

def initGlobals():
   glob = ParametersWardrobe('xsvt-globals')
   #This gets from Redis a set of variables called xsvt-globals.
   #There might be many defined, there might be none.
   rootDir=getRootDir("/data/id15b/inhouse/xog","lenses")
   glob.add('rootDir',rootDir)

def showGlobals():
   glob = ParametersWardrobe('xsvt-globals')
   print(glob.keys())
   print(glob.items())

def requiredGlobals():
   missing=0
   glob = ParametersWardrobe('xsvt-globals')
   try:
      glob.baslerInPos
   except AttributeError:
      missing+=1
      print("glob.baslerInPos not known yet.")
      print("   Move Basler into the beam and type '<macroset>.setBaslerIn()' !")
   try:
      glob.baslerNearPos
   except AttributeError:
      missing+=1
      print("glob.baslerNearPos not known yet.")
      print("   Move Basler into the beam AS FAR UPSTREAM and type '<macroset>.setBaslerNear()' !")
   try:
      glob.baslerFarPos
   except AttributeError:
      missing+=1
      print("glob.baslerFarPos not known yet.")
      print("   Move Basler into the beam AS FAR DOWNSTREAM and type '<macroset>.setBaslerFar()' !")

   try:
      glob.wireInPos
   except AttributeError:
      missing+=1
      print("glob.wireInPos not known yet.")
      print("   Move wire cross into the beam and type '<macroset>.setWireIn()' !")
   try:
      glob.wireNearPos
   except AttributeError:
      missing+=1
      print("glob.wireNearPos not known yet.")
      print("   Move wire into the beam AS FAR UPSTREAM and type '<macroset>.setWireNear()' !")
   try:
      glob.wireFarPos
   except AttributeError:
      missing+=1
      print("glob.wireFarPos not known yet.")
      print("   Move wire into the beam AS FAR DOWNSTREAM and type '<macroset>.setWireFar()' !")

   try:
      glob.diodeInPos
   except AttributeError:
      missing+=1
      print("glob.diodeInPos not known yet.")
      print("   Move diode into the beam and type '<macroset>.setDiodeIn()' !")
  
   return missing

################################################################################
#####  SAVING NAMES AND FOLDERS    #############################################
################################################################################
################################################################################

SCAN_SAVING.base_path='/data/id30b/inhouse/XOG/2DLenses'

def saveAlign():
    SCAN_SAVING.template="align"
    SCAN_SAVING.images_path_template='scan{scan_number}'

def save(myfolder,myprefix):
    SCAN_SAVING.template=myfolder
    SCAN_SAVING.images_path_template='%s{scan_number}' % myprefix

def myDir(*args):
   glob=ParametersWardrobe('xsvt-globals')
   Nargs=len(args)   
   if Nargs==1:
      SCAN_SAVING.template=args[0]
   if Nargs>1:
      print("No or one arguments only allowed!")
   print("currently saving to folder %s " % SCAN_SAVING.template)
   try:
      print("(current lens set path is  %s )" % glob.lensSubfolder)
   except:
      pass
      
def myPointerFile(*args):
   glob=ParametersWardrobe('xsvt-globals')
   Nargs=len(args)   
   if Nargs==1:
      SCAN_SAVING.data_filename=args[0]
   if Nargs>1:
      print("No or one arguments only allowed!")
   print("currently h5 pointer file %s.h5 " % SCAN_SAVING.data_filename)

def mySave(dirname,scanname):
   myDir(dirname)
   myPointerFile(dirname)
   SCAN_SAVING.images_path_template=scanname
   SCAN_SAVING.images_prefix=scanname+"_"

def endSaving():
   myDir("align")
   myPointerFile("align")
   SCAN_SAVING.images_path_template=("align_scans")
   SCAN_SAVING.images_prefix=("align_scan_")

################################################################################
###   DEFAULT POSITIONS  (GLOBAL VARIABLES)   ##################################
################################################################################
################################################################################

def setBaslerIn(*args):
   glob=ParametersWardrobe('xsvt-globals')
   Nargs=len(args)
   if Nargs==0:
      baslerInPos=[(diagy.name,diagy.position)]   #tuple of motor and motor position
      baslerInPos.append((diagz.name,diagz.position))
   else:
      baslerInPos=[]
      for motor in args:
         baslerInPos.append((motor.name,motor.position))
   glob.add('baslerInPos',baslerInPos)
   showBaslerIn()             

def showBaslerIn():
   glob=ParametersWardrobe('xsvt-globals')
   try:
      glob.baslerInPos
   except AttributeError:
      print("glob.baslerInPos not known yet.")
      print("Use setBaslerIn() first!")
      stopCode=1
   else:
      stopCode=0
   if stopCode:
      raise KeyboardInterrupt
   print("Basler In at:")
   for myMotor in glob.baslerInPos:
      print("  Motor %s at %.3f" % (myMotor[0],myMotor[1]))

def baslerIn():
   glob=ParametersWardrobe('xsvt-globals')
   showBaslerIn()
   print("Moving Basler into the beam.")
   for motor in glob.baslerInPos:
      umv(config.get(motor[0]),motor[1])  
   psBasler()

def setBaslerNear(*args):
   glob=ParametersWardrobe('xsvt-globals')
   Nargs=len(args)
   if Nargs==0:
      baslerNearPos=[(diagy.name,diagy.position)]   #tuple of motor and motor position
      baslerNearPos.append((diagz.name,diagz.position))
      baslerNearPos.append((diagx.name,diagx.position))
   else:
      baslerNearPos=[]
      for motor in args:
         baslerNearPos.append((motor.name,motor.position))
   glob.add('baslerNearPos',baslerNearPos)
   showBaslerNear()             

def showBaslerNear():
   glob=ParametersWardrobe('xsvt-globals')
   try:
      glob.baslerNearPos
   except AttributeError:
      print("glob.baslerNearPos not known yet.")
      print("Use setBaslerNear() first!")
      stopCode=1
   else:
      stopCode=0
   if stopCode:
      raise KeyboardNearterrupt
   print("Basler Near at:")
   for myMotor in glob.baslerNearPos:
      print("  Motor %s at %.3f" % (myMotor[0],myMotor[1]))

def baslerNear():
   glob=ParametersWardrobe('xsvt-globals')
   showBaslerNear()
   print("Moving Basler into the beam.")
   for motor in glob.baslerNearPos:
      umv(config.get(motor[0]),motor[1])  
   psBasler()

def setBaslerFar(*args):
   glob=ParametersWardrobe('xsvt-globals')
   Nargs=len(args)
   if Nargs==0:
      baslerFarPos=[(diagy.name,diagy.position)]   #tuple of motor and motor position
      baslerFarPos.append((diagz.name,diagz.position))
      baslerFarPos.append((diagx.name,diagx.position))
   else:
      baslerFarPos=[]
      for motor in args:
         baslerFarPos.append((motor.name,motor.position))
   glob.add('baslerFarPos',baslerFarPos)
   showBaslerFar()             

def showBaslerFar():
   glob=ParametersWardrobe('xsvt-globals')
   try:
      glob.baslerFarPos
   except AttributeError:
      print("glob.baslerFarPos not known yet.")
      print("Use setBaslerFar() first!")
      stopCode=1
   else:
      stopCode=0
   if stopCode:
      raise KeyboardFarterrupt
   print("Basler Far at:")
   for myMotor in glob.baslerFarPos:
      print("  Motor %s at %.3f" % (myMotor[0],myMotor[1]))

def baslerFar():
   glob=ParametersWardrobe('xsvt-globals')
   showBaslerFar()
   print("Moving Basler into the beam.")
   for motor in glob.baslerFarPos:
      umv(config.get(motor[0]),motor[1])  
   psBasler()


def setWireIn(*args):
   glob=ParametersWardrobe('xsvt-globals')
   Nargs=len(args)
   if Nargs==0:
      wireInPos=[(diagy.name,diagy.position)]   #tuple of motor and motor position
      wireInPos.append((diagz.name,diagz.position))
   else:
      wireInPos=[]
      for motor in args:
         wireInPos.append((motor.name,motor.position))
   glob.add('wireInPos',wireInPos)
   showWireIn()             

def showWireIn():
   glob=ParametersWardrobe('xsvt-globals')
   try:
      glob.wireInPos
   except AttributeError:
      print("glob.wireInPos not known yet.")
      print("Use setWireIn() first!")
      stopCode=1
   else:
      stopCode=0
   if stopCode:
      raise KeyboardInterrupt
   print("Wire In at:")
   for myMotor in glob.wireInPos:
      print("  Motor %s at %.3f" % (myMotor[0],myMotor[1]))

def wireIn():
   glob=ParametersWardrobe('xsvt-globals')
   showWireIn()
   print("Moving Wire into the beam.")
   for motor in glob.wireInPos:
      umv(config.get(motor[0]),motor[1])  
   psDiode()

def setWireNear(*args):
   glob=ParametersWardrobe('xsvt-globals')
   Nargs=len(args)
   if Nargs==0:
      wireNearPos=[(diagy.name,diagy.position)]   #tuple of motor and motor position
      wireNearPos.append((diagz.name,diagz.position))
      wireNearPos.append((diagx.name,diagx.position))
   else:
      wireNearPos=[]
      for motor in args:
         wireNearPos.append((motor.name,motor.position))
   glob.add('wireNearPos',wireNearPos)
   showWireNear()             

def showWireNear():
   glob=ParametersWardrobe('xsvt-globals')
   try:
      glob.wireNearPos
   except AttributeError:
      print("glob.wireNearPos not known yet.")
      print("Use setWireNear() first!")
      stopCode=1
   else:
      stopCode=0
   if stopCode:
      raise KeyboardNearterrupt
   print("Wire Near at:")
   for myMotor in glob.wireNearPos:
      print("  Motor %s at %.3f" % (myMotor[0],myMotor[1]))

def wireNear():
   glob=ParametersWardrobe('xsvt-globals')
   showWireNear()
   print("Moving Wire into the beam.")
   for motor in glob.wireNearPos:
      umv(config.get(motor[0]),motor[1])  
   psDiode()

def setWireFar(*args):
   glob=ParametersWardrobe('xsvt-globals')
   Nargs=len(args)
   if Nargs==0:
      wireFarPos=[(diagy.name,diagy.position)]   #tuple of motor and motor position
      wireFarPos.append((diagz.name,diagz.position))
      wireFarPos.append((diagx.name,diagx.position))
   else:
      wireFarPos=[]
      for motor in args:
         wireFarPos.append((motor.name,motor.position))
   glob.add('wireFarPos',wireFarPos)
   showWireFar()             

def showWireFar():
   glob=ParametersWardrobe('xsvt-globals')
   try:
      glob.wireFarPos
   except AttributeError:
      print("glob.wireFarPos not known yet.")
      print("Use setWireFar() first!")
      stopCode=1
   else:
      stopCode=0
   if stopCode:
      raise KeyboardFarterrupt
   print("Wire Far at:")
   for myMotor in glob.wireFarPos:
      print("  Motor %s at %.3f" % (myMotor[0],myMotor[1]))

def wireFar():
   glob=ParametersWardrobe('xsvt-globals')
   showWireFar()
   print("Moving Wire into the beam.")
   for motor in glob.wireFarPos:
      umv(config.get(motor[0]),motor[1])  
   psDiode()

def setDiodeIn(*args):
   glob=ParametersWardrobe('xsvt-globals')
   Nargs=len(args)
   if Nargs==0:
      diodeInPos=[(diagy.name,diagy.position)]   #tuple of motor and motor position
      diodeInPos.append((diagz.name,diagz.position))
   else:
      diodeInPos=[]
      for motor in args:
         diodeInPos.append((motor.name,motor.position))
   glob.add('diodeInPos',diodeInPos)
   showDiodeIn()             

def showDiodeIn():
   glob=ParametersWardrobe('xsvt-globals')
   try:
      glob.diodeInPos
   except AttributeError:
      print("glob.diodeInPos not known yet.")
      print("Use setDiodeIn() first!")
      stopCode=1
   else:
      stopCode=0
   if stopCode:
      raise KeyboardInterrupt
   print("Diode In at:")
   for myMotor in glob.diodeInPos:
      print("  Motor %s at %.3f" % (myMotor[0],myMotor[1]))

def diodeIn():
   glob=ParametersWardrobe('xsvt-globals')
   showDiodeIn()
   print("Moving Diode into the beam.")
   for motor in glob.diodeInPos:
      umv(config.get(motor[0]),motor[1])  
   psDiode()

################################################################################
#####  PLOTSELECT DIODE AND BASLER    ##########################################
################################################################################
################################################################################

def psBasler():
   enable(basler)
   plotselect(basler.counters.intensity)
   plotselect('basler:bpm:intensity')

def psBxy()
   enable(basler)
   plotselect(basler.counters.x,basler.counters.y)
   plotselect('basler:bpm:x','basler:bpm:y')

def psFwhm()
   enable(basler)
   plotselect(basler.counters.fwhmx,basler.counters.fwhmy)
   plotselect('basler:bpm:fwhm_x','basler:bpm:fwhm_y')

def psDiode()
   disable(basler)
   plotselect(kdiode.counters.kdiode)

#    mg1.enable_all()
#    mg1.disable("kdiode:kdiode") 
#    plotselect('xrayeye:bpm:fwhm_y','xrayeye:bpm:fwhm_x')  
#    plotselect('xrayeye:bpm:y','xrayeye:bpm:x')  
#    plotselect('xrayeye:bpm:intensity')  
#    mg1.disable('xrayeye:bpm:acq_time', 'xrayeye:bpm:intensity', 'xrayeye:bpm:fwhm_x', 'xrayeye:bpm:y', 'xrayeye:bpm:fwhm_y', 'xrayeye:image', 'xrayeye:bpm:x')
#    plotselect(kdiode.counters.kdiode)

################################################################################
################################################################################
################################################################################
################################################################################
   
def baslerWaypoints():
   glob=ParametersWardrobe('xsvt-globals')
   try:
      pos1=glob.baslerNearPos
      pos2=glob.baslerFarPos
      (x1,y1,z1)=_getxyz(pos1)
      (x2,y2,z2)=_getxyz(pos2)
      print("Basler waypoint 1 defined as:")
      print("   diagx= %7.2f " % x1)
      print("   diagy= %7.2f " % y1)
      print("   diagz= %7.2f " % z1)
      print()
      print("Basler waypoint 2 defined as:")
      print("   diagx= %7.2f " % x2)
      print("   diagy= %7.2f " % y2)
      print("   diagz= %7.2f " % z2)
   except:
      print("No waypoints defined. Use setBaslerNear and  setBaslerFar")

def baslerX(xpos=25):
   glob=ParametersWardrobe('xsvt-globals')
   try:
      positions0=glob.baslerInPos
   except:
      showBaslerIn()
   try:
      positions1=glob.baslerNearPos
      positions2=glob.baslerFarPos
   except:
      print("Do not know Basler positions at close and far distance.")
      print("no parasitic movement can be corrected for. Do a simple move")
      #get diagx from command argument xpos
      #get diagz and diagy from parameters Wardrobe
      allmotors=list(range(len(glob.baslerInPos)))
      ii=0
      for motor in glob.baslerInPos:
         if motor[0]!="diagx":
            allmotors[ii]=(motor[0],motor[1])
            ii+=1
      allmotors.append(("diagx",xpos))
      umv(config.get(allmotors[0][0]),allmotors[0][1],\
          config.get(allmotors[1][0]),allmotors[1][1],\
          config.get(allmotors[2][0]),allmotors[2][1])  
      return
   #get diagx from command argument xpos
   #get diagz and diagy from linear fit between extremes 
   (x1,y1,z1)=_getxyz(positions1)
   (x2,y2,z2)=_getxyz(positions2)

   x=xpos
   y=y1+((x-x1)/(x2-x1))*(y2-y1)
   z=z1+((x-x1)/(x2-x1))*(z2-z1)
   umv(diagx,x,diagy,y,diagz,z)

def wireWaypoints():
   glob=ParametersWardrobe('xsvt-globals')
   try:
      pos1=glob.wireNearPos
      pos2=glob.wireFarPos
      (x1,y1,z1)=_getxyz(pos1)
      (x2,y2,z2)=_getxyz(pos2)
      print("Wire waypoint 1 defined as:")
      print("   diagx= %7.2f " % x1)
      print("   diagy= %7.2f " % y1)
      print("   diagz= %7.2f " % z1)
      print()
      print("Wire waypoint 2 defined as:")
      print("   diagx= %7.2f " % x2)
      print("   diagy= %7.2f " % y2)
      print("   diagz= %7.2f " % z2)
   except:
      print("No waypoints defined. Use setWireNear and  setWireFar")

def wireX(xpos=25):
   glob=ParametersWardrobe('xsvt-globals')
   try:
      positions0=glob.wireInPos
   except:
      showWireIn()
   try:
      positions1=glob.wireNearPos
      positions2=glob.wireFarPos
   except:
      print("Do not know Wire positions at close and far distance.")
      print("no parasitic movement can be corrected for. Do a simple move")
      #get diagx from command argument xpos
      #get diagz and diagy from parameters Wardrobe
      allmotors=list(range(len(glob.wireInPos)))
      ii=0
      for motor in glob.wireInPos:
         if motor[0]!="diagx":
            allmotors[ii]=(motor[0],motor[1])
            ii+=1
      allmotors.append(("diagx",xpos))
      umv(config.get(allmotors[0][0]),allmotors[0][1],\
          config.get(allmotors[1][0]),allmotors[1][1],\
          config.get(allmotors[2][0]),allmotors[2][1])  
      return
   #get diagx from command argument xpos
   #get diagz and diagy from linear fit between extremes 
   (x1,y1,z1)=_getxyz(positions1)
   (x2,y2,z2)=_getxyz(positions2)

   x=xpos
   y=y1+((x-x1)/(x2-x1))*(y2-y1)
   z=z1+((x-x1)/(x2-x1))*(z2-z1)
   umv(diagx,x,diagy,y,diagz,z)

################################################################################
################################################################################
################################################################################
################################################################################
def _getxyz(pos):
   for item in pos:
      if item[0]=="diagx":
          x=item[1]
      if item[0]=="diagy":
          y=item[1]
      if item[0]=="diagz":
          z=item[1]
   return (x,y,z)

def fwy(x):
   try:
      positions1=glob.wireNearPos
      positions2=glob.wireFarPos
   except:
      print("NO WIRE NEAR AND FAR POSITIONS DEFINED")
      raise KeyboardInterrupt
   (x1,y1,z1)=_getxyz(positions1)
   (x2,y2,z2)=_getxyz(positions2)
   return y1+((x-x1)/(x2-x1))*(y2-y1)

def fwz(x):
   try:
      positions1=glob.wireNearPos
      positions2=glob.wireFarPos
   except:
      print("NO WIRE NEAR AND FAR POSITIONS DEFINED")
      raise KeyboardInterrupt
   (x1,y1,z1)=_getxyz(positions1)
   (x2,y2,z2)=_getxyz(positions2)
   return z1+((x-x1)/(x2-x1))*(z2-z1)

def fby(x):
   try:
      positions1=glob.baslerNearPos
      positions2=glob.baslerFarPos
   except:
      print("NO BASLER NEAR AND FAR POSITIONS DEFINED")
      raise KeyboardInterrupt
   (x1,y1,z1)=_getxyz(positions1)
   (x2,y2,z2)=_getxyz(positions2)
   return y1+((x-x1)/(x2-x1))*(y2-y1)

def fbz(x):
   try:
      positions1=glob.baslerNearPos
      positions2=glob.baslerFarPos
   except:
      print("NO BASLER NEAR AND FAR POSITIONS DEFINED")
      raise KeyboardInterrupt
   (x1,y1,z1)=_getxyz(positions1)
   (x2,y2,z2)=_getxyz(positions2)
   return z1+((x-x1)/(x2-x1))*(z2-z1)

def baslerscan(x1,x2,pts,cttime=0.0005):
   a3scan(diagx,x1,x2,diagy,fby(x1),fby(x2),diagz,fbz(x1),fbz(x2),pts,cttime)
        
def image_meanmin_meanmax(arr,percentage):
   s_arr=np.sort(arr,axis=None, kind='quicksort', order=None)
   size_vec=int(percent*s_arr.size)
   mean_min=np.mean(s_arr[0:size_vect])
   mean_max=np.mean(s_arr[-size_vect:-1])   
   return mean_min, mean_max

def caustic(newdir,dist1,dist2,steps,exptime,correctLateral=True,correctExptime=False,comeBack=False,settleT=2):
   startX=diagx.position
   startY=diagy.position
   startZ=diagz.position
   glob=ParametersWardrobe('xsvt-globals')
   SCAN_SAVING.template=newdir
   SCAN_SAVING.data_filename=newdir
   SCAN_SAVING.images_path_template="causticScan"
   SCAN_SAVING.images_prefix="causticScan_"
   if correctExptime:
      cttime=exptime
      for xx in np.linspace(dist1,dist2,steps+1):
         if correctLateral:
            umv(diagx,xx,diagy,fby(xx),diagz,fbz(xx))
         else:
            umv(diagx,xx)
         #moved, now count:
         myscan=sct(cttime,glob.camera,glob.camera.counters)
         mydata=myscan.get_data()
         arr=mydata["image"].as_array()
         arrLowMean,arrHighMean=image_meanmin_meanmax(arr,0.01)
         arrMax=nparr.max()
         #adjust exptime
         if (arrMax > 10000):
            cttime=cttime*0.5
         elif (arrMax < 2000) and cttime*2<exptime:
            cttime=cttime*2
   else:
      if correctLateral:
         a3scan(diagx,dist1,dist2,diagy,fby(dist1),fby(dist2),diagz,fbz(dist1),fbz(dist2),steps,exptime,sleep_time=settleT)
      else:
         ascan(diagx,dist1,dist2,steps,exptime,sleep_time=settleT)      
   if comeBack:
      print("Moving back to where we were at the beginning of this macro...")
      umv(diagx,startX,diagy,startY,diagz,startZ)
   endSaving()


####################################################################################
####  NEW WIRE SCAN STUFF  #########################################################
####################################################################################
####################################################################################

def wireScan(mmScanwidth=0.4,xpos=None;filename="wirescan.dat",nmStepsize=500,quadrant="ur",diameter=0.2,yoff=0.3,zoff=0.3,fullWire=True,readjust=False):
   if quadrant not in ["ur","dr","ul","dl","UR","DR","UL","DL"]:
      print("ALLOWED QUADRANTS ARE:")
      print("ur, dr, ul, dl, UR, DR, UL, DL")
      raise KeyboardInterrupt
   if xpos==None:
      xpos=diagx.position  


   #umv(wirez,zoff)
   #dscan(wirey,)

   #umv(wirey,yoff)
   #dscan(wirez,)

   #edgefity
   #edgefitx
   #write to file


def edgefit(xarr,yarr):
   pass

def wirePlot(filename="wirescan.dat",skiplines=0):
   pass


####################################################################################
####  OLD WIRE SCAN STUFF  #########################################################
####################################################################################
####################################################################################

def oldwscanh(x=None,repeats=1):
    if x==None:
       x=diagx.position
    wireIn()
    umv(wirez,0.5,wirey,0)
    for ii in range(repeats):
       ascan(wirey,-0.3,0.3,300,.2)
       ascan(wirey,-0.2,-0.0,200,.2)
       ascan(wirey,0.0,0.2,200,.2)
       umv(wirey,0)

def oldwscanv(x=None,repeats=1):
    if x==None:
       x=diagx.position
    wireIn()
    umv(wirey,0.5,wirez,0)
    for ii in range(repeats):
       ascan(wirez,-0.3,0.3,300,.2)
       ascan(wirez,-0.2,-0.05,150,.2)
       ascan(wirez,0.05,0.2,150,.2)
       umv(wirez,0)

def oldwirescansZ(newdir,Xdist1,Xdist2,Xsteps,wire1,wire2,wiresteps,exptime=0.2,pico=pico1,comeBack=False):
   startX3=diagx.position
   startWireZ=wirez.position
   glob=ParametersWardrobe('xsvt-globals')
   SCAN_SAVING.template=newdir
   SCAN_SAVING.data_filename=newdir
   SCAN_SAVING.images_path_template="wireZScan"
   SCAN_SAVING.images_prefix="wireZScan_"
   umv(y3,-227.85,wirey,0.5)
   for xx in np.linspace(Xdist1,Xdist2,Xsteps):
      umv(x3,xx)
      sleep(1)
      ascan(wirez,wire1,wire2,wiresteps,exptime,pico)
   if comeBack:
      print("Moving back to where we were at the beginning of this macro...")
      umv(diagx,startX3,wirez,startWireZ)
   endSaving()

def oldwirescansY(newdir,Xdist1,Xdist2,Xsteps,wire1,wire2,wiresteps,exptime=0.2,pico=pico1,comeBack=False):
   startX3=diagx.position
   startWireY=wirey.position
   glob=ParametersWardrobe('xsvt-globals')
   SCAN_SAVING.template=newdir
   SCAN_SAVING.data_filename=newdir
   SCAN_SAVING.images_path_template="wireYScan"
   SCAN_SAVING.images_prefix="wireYScan_"
   umv(y3,-227.85,wirez,0.5)
   for xx in np.linspace(Xdist1,Xdist2,Xsteps):
      umv(x3,xx)
      sleep(1)
      ascan(wirey,wire1,wire2,wiresteps,exptime,pico)
   if comeBack:
      print("Moving back to where we were at the beginning of this macro...")
      umv(diagx,startX3,wirey,startWireY)
   endSaving()


###################################################################
####  ENERGY VARIATIONS  ##########################################
###################################################################
###################################################################
       
def baslerEscan(E1,E2,Esteps):
    save("N9_Escan","N9_Escan")
    cttime=0.0002
    for EE in np.linspace(E1,E2,Esteps):
        print(EE)
        move_energy(EE)
        sleep(1)
        result=ct(cttime)
        Ib=result.get_data("intensity")[0]
        if Ib>4000:
            cttime=cttime/2.0
        if Ib<1500:
            cttime=cttime*2.0
        sct(cttime)

###################################################################
####  TD AND BEAM   ###############################################
###################################################################
###################################################################

def td(searchstr=None):
   if searchstr==None:
       os.system("td papillon")
       os.system("td hanfland")
       os.system("td guilloud")
       os.system("td garbarino")
   else
       os.system("td %s" % searchstring) 
    
def beam():
   fshutter.open()
   safshut1.open()
   shstate=str(safshut2.state).split(".")[-1]
   if shstate=="OPEN":
      print("Shutter for eh1 is already open")
      return
   if shstate=="DISABLED":
      print("Currently disabled. Waiting for end of search ...")
      while shstate=="DISABLED":
        sleep(.1)
        shstate=str(safshut2.state).split(".")[-1]
   print("Opening the shutter...")
   ctr=0
   while shstate!="OPEN":
      safshut2.open()
      if (ctr>2):
         sleep(.25)
         shstate=str(safshut2.state).split(".")[-1]
      else:
         shstate=str(safshut2.state).split(".")[-1]
      ctr+=1
         
###################################################################
###################################################################
###################################################################
###################################################################

initGlobals()
glob = ParametersWardrobe('xsvt-globals')
gevent.sleep(1)

print("XOGatID15B.py loaded")
print("Root directory is %s" % glob.rootDir)

current_session.disable_esrf_data_policy()
SCAN_SAVING.base_path=glob.rootDir
