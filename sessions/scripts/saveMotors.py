def saveMotors():
    """
    Sava all motor positions in both user and dial units
    """
    nowStr=time.strftime("%Y%m%d-%Hh%M")
    outFile="%s_motorPos_%s" % (SCAN_SAVING.session,nowStr)
    print("Positions will be saved to file: %s" % outFile)
    
    header, pos, dial = [], [], []
    tables = [(header, pos, dial)]
    errors = []

    data = iter_axes_position_all()
    for axis, disabled, error, axis_unit, position, dial_position in data:

        axis_label = axis.name
        if axis_unit:
            axis_label += "[{0}]".format(axis_unit)

        header.append(axis_label)
        if error:
            errors.append((axis.name, error))
            position = dial_position = _ERR
        if disabled:
            position = dial_position = _DIS
        pos.append(rounder(axis.tolerance, position))
        dial.append(rounder(axis.tolerance, dial_position))

        _print_errors_with_traceback(errors, device_type="motor")

    for table in tables:
        print("")
        print(_tabulate(table, disable_numparse=True, stralign="right"))
