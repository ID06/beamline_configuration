
from bliss.setup_globals import *

load_script("session_pie727.py")

print("")
print("Welcome to your new 'session_pie727' BLISS session !! ")
print("")
print("You can now customize your 'session_pie727' session by changing files:")
print("   * /session_pie727_setup.py ")
print("   * /session_pie727.yml ")
print("   * /scripts/session_pie727.py ")
print("")