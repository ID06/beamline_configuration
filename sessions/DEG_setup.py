from bliss.setup_globals import *
import numpy as np
import sys
import os
import time
from bliss import current_session
from bliss.shell.standard import plotselect as ps

load_script("DEG.py")

print("")
print("Welcome to your new 'DEG' BLISS session !! ")
print("")
print("You can now customize your 'DEG' session by changing files:")
print("   * /DEG_setup.py ")
print("   * /DEG.yml ")
print("   * /scripts/DEG.py ")
print("")

from id06.eh2presets import (
    CountFastShutterPreset, 
    FScanFastShutterPreset,
)

print("Setup fast shutter in the chain acquisition.\n")
_countshut_DEG = CountFastShutterPreset(eh1fs)
DEFAULT_CHAIN.add_preset(_countshut_DEG, "fastshutter")


print("Loading T.Roth convenience macros ...")
user_script_load("/data/optics/refractive_optics/macros/tr.py","tr")
print("Loading ID06 EH1 convenience macros ...")
user_script_load("/users/blissadm/local/beamline_configuration/sessions/scripts/eh1.py","eh1")

def ISDDsaving():
  current_session.disable_esrf_data_policy()
  yearInt = time.localtime()[0]
  SCAN_SAVING.base_path = '/data/id06-hxm/inhouse/%4d' % yearInt
  SCAN_SAVING.template  = '{session}/{date}'
  print("SCAN_SAVING.base_path:")
  print(SCAN_SAVING.base_path)
  print("SCAN_SAVING.filename:")
  print(SCAN_SAVING.filename)

print("SCAN_SAVING.base_path:")
print(SCAN_SAVING.base_path)
print("SCAN_SAVING.filename:")
print(SCAN_SAVING.filename)
print()
print("TYPE \"ISDDsaving()\" TO CHANGE SCAN_SAVING FOR NOW !!!!!!!!!!")
o
